extern crate serde;
extern crate serde_json;

#[derive(Deserialize)]
#[derive(Debug)]
pub struct PushData {
    pub images: Vec<String>,
    pub pushed_at: f64,
    pub pusher: String,
    pub tag: String
}

#[derive(Deserialize)]
#[derive(Debug)]
pub struct Repository {
    pub status: String,
    pub description: String,
    pub is_trusted: bool,
    pub full_description: String,
    pub repo_url: String,
    pub owner: String,
    pub is_official: bool,
    pub is_private: bool,
    pub name: String,
    pub namespace: String,
    pub star_count: i8,
    pub comment_count: i8,
    pub date_created: f64,
    pub repo_name: String,
}

#[derive(Deserialize)]
#[derive(Debug)]
pub struct WebHookPayload {
    pub callback_url: String,
    pub push_data: PushData,
    pub repository: Repository
}