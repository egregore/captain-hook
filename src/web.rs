extern crate hyper;
extern crate futures;
extern crate serde;
extern crate serde_json;

use std::process::Command;
use futures::future::Future;
use futures::stream::Stream;
use serde_json::Error as JsonError;
use hyper::server::{Request, Response, Service};
use hyper::{Method, StatusCode, Error};
use std::str;

use lib::Bindings;
use payload::WebHookPayload;

pub struct Hook {
    pub token: String,
    pub bindings: Bindings
}

impl Service for Hook {
    type Request = Request;
    type Response = Response;
    type Error = Error;

    type Future = Box<Future<Item=Self::Response, Error=Self::Error>>;

    fn call(&self, req: Request) -> Self::Future {
        let (method, uri, _, _, body) = req.deconstruct();
        let ref authenticated_path = format!("/execute/{}", &self.token);
        let mut response = Response::new();
        let request_path = uri.path().to_string();
        let bindings = self.bindings.clone();

        match (method, request_path) {
            (Method::Post, ref matched) if matched == authenticated_path => {
                Box::new(
                    body.concat2()
                        .and_then(move |body| {
                            let stringify = str::from_utf8(&body).unwrap();
                            let parse_result: Result<WebHookPayload, JsonError> = serde_json::from_str(&stringify);

                            match parse_result {
                                Ok(payload) => {
                                    println!("Got request for {}:{}", payload.repository.repo_name, payload.push_data.tag);

                                    bindings.values.get(&payload.repository.repo_name)
                                        .map(|binding| binding.clone().script)
                                        .map(|script| {
                                            let command_result = Command::new("/bin/bash")
                                                .arg("-c")
                                                .arg(script)
                                                .output();

                                            match command_result {
                                                Ok(ok) => println!("Output:\n{}\n{}",
                                                                   String::from_utf8_lossy(&ok.stdout),
                                                                   String::from_utf8_lossy(&ok.stderr)
                                                ),
                                                Err(err) => println!("Error:\n{}", err)
                                            }
                                        })
                                        .or_else(|| {
                                            println!("No binding for {}", payload.repository.repo_name);
                                            None
                                        });
                                    futures::future::ok(response.with_status(StatusCode::Ok))
                                }
                                Err(json_error) => {
                                    println!("Invalid JSON payload body:\n{}", json_error);
                                    println!("Json in question:\n{}", stringify);
                                    futures::future::ok(response.with_status(StatusCode::BadRequest))
                                }
                            }
                        })
                )
            }
            _ => {
                response.set_status(StatusCode::NotFound);
                Box::new(futures::future::ok(response))
            }
        }
    }
}