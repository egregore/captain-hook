use std::collections::HashMap;

#[derive(Clone)]
pub struct Binding {
    pub repo: String,
    pub tag: String,
    pub script: String
}

impl Binding {
    fn parse(binding_string: &str) -> Result<Binding, &str> {
        let splat: Vec<&str> = binding_string.split("|").collect();

        match splat.len() {
            2 => {
                let repo = splat.get(0).unwrap().to_string();
                let script = splat.get(1).unwrap().to_string();
                if repo.contains(":") {
                    let repo_splat: Vec<&str> = repo.split(":").collect();
                    match repo_splat.len() {
                        2 => Ok(Binding {
                            repo: repo_splat.get(0).unwrap().to_string(),
                            tag: repo_splat.get(1).unwrap().to_string(),
                            script
                        }),
                        _ => Err(binding_string.clone())
                    }
                } else {
                    Ok(Binding {
                        repo,
                        tag: "latest".to_string(),
                        script
                    })
                }
            }
            _ => Err(binding_string.clone())
        }
    }
}

#[derive(Clone)]
pub struct Bindings {
    pub values: HashMap<String, Binding>
}

impl Bindings {
    pub fn parse(binding_strings: Vec<&str>) -> Result<Bindings, Vec<&str>> {
        let bindings_results: Vec<Result<Binding, &str>> = binding_strings.iter()
            .map(|binding_string| Binding::parse(binding_string))
            .collect();

        let (errors, bindings): (Vec<Result<Binding, &str>>, Vec<Result<Binding, &str>>) = bindings_results.into_iter()
            .partition(|ref res| res.is_err());

        if !errors.is_empty() {
            Err(errors.into_iter().map(|e| e.err().unwrap()).collect())
        } else {
            //Ok(Bindings { values: bindings.into_iter().map(|r| r.unwrap()).collect() })
            Ok(Bindings {
                values: bindings.into_iter().fold(HashMap::new(), |mut map, res| {
                    let binding = res.unwrap();
                    map.insert(binding.repo.to_string(), binding);
                    map
                })
            })
        }
    }
}