extern crate hyper;
extern crate futures;
extern crate clap;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

use hyper::server::Http;

use clap::{Arg, App};
use std::process;

mod web;
mod lib;
mod payload;

fn main() {
    let command_line_args = App::new("hook")
        .version("0.1.0")
        .author("Łukasz Biały <lukasz.marcin.bialy@gmail.com>")
        .about("Calls commands in response to docker hub hook calls.")
        .arg(Arg::with_name("token")
            .short("t")
            .long("token")
            .value_name("TOKEN")
            .help("Private key to guard endpoint")
            .takes_value(true)
            .required(true))
        .arg(Arg::with_name("port")
            .short("p")
            .long("port")
            .value_name("PORT")
            .help("Port to bind http server to")
            .takes_value(true)
            .required(true))
        .arg(Arg::with_name("bindings")
            .short("b")
            .long("bindings")
            .value_name("BINDINGS_STRING")
            .help("Binding strings of form <dockerhub/repo:tag>|script to execute on match")
            .value_delimiter(",")
            .require_delimiter(true)
            .takes_value(true))
        .get_matches();

    let token = String::from(command_line_args.value_of("token").unwrap_or_else(|| {
        println!("You have to provide hook token!");
        process::exit(1);
    }));
    let port = command_line_args.value_of("port").unwrap_or("3000");
    let binding_strings = command_line_args.values_of("bindings")
        .map(|i| { i.collect() })
        .unwrap_or(vec![]);

    if binding_strings.is_empty() {
        println!("You need to pass at least one binding. Exiting.");
        process::exit(1);
    }

    match lib::Bindings::parse(binding_strings) {
        Err(errors) => {
            println!("These {} bindings are invalid:", errors.len());
            for bad_binding in errors {
                println!("> {}", bad_binding);
            }
            process::exit(1);
        }
        Ok(bindings) => {
            for (_, binding) in bindings.values.clone() {
                println!("Binding - repo: {}, tag: {}, script: {}", binding.repo, binding.tag, binding.script);
            }

            let local_address = format!("0.0.0.0:{}", port);

            let socket_address = local_address.parse().unwrap(); // unsafe

            println!("Listening on {}", local_address);

            let server = Http::new()
                .bind(&socket_address, move || Ok(web::Hook {
                    bindings: bindings.clone(),
                    token: token.to_string()
                }))
                .unwrap(); // unsafe

            server.run().unwrap(); // unsafe
        }
    }
}